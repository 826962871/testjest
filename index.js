const qs = require("qs")
const stripKeysFromSearch = (keys = [], config = {}) => {
  const { search } = global.location
  if (!search) {
    return ""
  }
  try {
    const defaultConfig = {
      all: true,
    }
    const options = Object.assign({}, defaultConfig, config)
    const queryObj = qs.parse(search.slice(1))
    if (options && options.all) {
      const needDel =
        Object.keys(queryObj).filter(key => keys.indexOf(key) !== -1).length ===
        keys.length
      if (needDel) {
        keys.forEach(key => {
          delete queryObj[key]
        })
      }
    } else {
      keys.forEach(key => {
        delete queryObj[key]
      })
    }
    const query = `?${qs.stringify(queryObj)}`
    return query
  } catch (e) {
    console.log(e)
  }
  return search
}

module.exports = stripKeysFromSearch
