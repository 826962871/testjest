const stripKeysFromSearch = require("./index")
const utils = {
  stripKeysFromSearch,
}
describe("stripKeysFromSearch 测试", () => {
  test("空字符串原样返回", () => {
    const search = ""
    Object.defineProperty(global, "location", {
      value: {
        search,
      },
      writable: true,
    })
    expect(utils.stripKeysFromSearch(["code", "state"])).toEqual(search)
  })
  test("search只包含指定的key", () => {
    let search = "?code=0811H9Jc2Bf4HG0kzQIc2GQ8Jc21H9JS&state=STATE"
    Object.defineProperty(global, "location", {
      value: {
        search,
      },
      writable: true,
    })
    expect(utils.stripKeysFromSearch(["code", "state"])).toEqual("?")
  })
  test("search后部包含指定keys以外的key", () => {
    let search =
      "?code=0811H9Jc2Bf4HG0kzQIc2GQ8Jc21H9JS&state=STATE&activity_id=142"
    Object.defineProperty(global, "location", {
      value: {
        search,
      },
      writable: true,
    })
    expect(utils.stripKeysFromSearch(["code", "state"])).toEqual(
      "?activity_id=142"
    )
  })
  test("search前部包含指定keys以外的key", () => {
    let search =
      "?activity_id=142&code=0811H9Jc2Bf4HG0kzQIc2GQ8Jc21H9JS&state=STATE"
    Object.defineProperty(global, "location", {
      value: {
        search,
      },
      writable: true,
    })
    expect(utils.stripKeysFromSearch(["code", "state"])).toEqual(
      "?activity_id=142"
    )
  })
  test("search前后部包含指定keys以外的key", () => {
    let search =
      "?activity_id=142&code=0811H9Jc2Bf4HG0kzQIc2GQ8Jc21H9JS&state=STATE&foo=bar"
    Object.defineProperty(global, "location", {
      value: {
        search,
      },
      writable: true,
    })
    expect(utils.stripKeysFromSearch(["code", "state"])).toEqual(
      "?activity_id=142&foo=bar"
    )
  })
  test("search只部分包含指定keys时，原样返回", () => {
    let search =
      "?activity_id=142&code=0811H9Jc2Bf4HG0kzQIc2GQ8Jc21H9JS&foo=bar"
    Object.defineProperty(global, "location", {
      value: {
        search,
      },
      writable: true,
    })
    expect(utils.stripKeysFromSearch(["code", "state"])).toEqual(search)
  })
  test("search只部分包含指定keys，并指定option为{all: false}, 删除匹配到的key", () => {
    let search =
      "?activity_id=142&code=0811H9Jc2Bf4HG0kzQIc2GQ8Jc21H9JS&foo=bar"
    Object.defineProperty(global, "location", {
      value: {
        search,
      },
      writable: true,
    })
    expect(
      utils.stripKeysFromSearch(["code", "state"], { all: false })
    ).toEqual("?activity_id=142&foo=bar")
  })
})
